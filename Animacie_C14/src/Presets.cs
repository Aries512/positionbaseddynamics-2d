﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtendedExtensionMethods;

namespace Animacie_C14 {
	class Presets {

		public const int TWO_INF = 0;
		public const int ONE_INF = 1;
		public const int COLLISIONS1 = 2;
		public const int COLLISIONS2 = 3;
		public const int LONG = 4;
		public const int BALLS = 5;

		public static SpringSystem Get(int idx) {
			if (idx == TWO_INF) {
				return generate(
					new List<double> {
						125, 200, Particle.INF_MASS,
						210, 100, 0.3,
						270, 100, 1,
						320, 150, 1,
						410, 200, Particle.INF_MASS
					},
					new List<int> {
						0, 1,
						1, 2,
						2, 3,
						3, 4
					});

			} else if (idx == ONE_INF) {
				return generate(
					new List<double> {
						250, 200, Particle.INF_MASS,
						360, 220, 1,
						430, 220, 1,
						500, 200, 1,
					},
					new List<int> {
						0, 1,
						1, 2,
						2, 3,
					});

			} else if (idx == COLLISIONS1) {
				return generate(
					new List<double> {
						150, 100, Particle.INF_MASS,
						50, 100, 1,
						170, 100, Particle.INF_MASS,
						170, 200, 1,
						190, 100, Particle.INF_MASS,
						190, 200, 1,
						210, 100, Particle.INF_MASS,
						210, 200, 1,

						350, 100, Particle.INF_MASS,
						250, 100, 1,
						370, 100, Particle.INF_MASS,
						370, 200, 0.6,
						390, 100, Particle.INF_MASS,
						390, 200, 0.3,
						410, 100, Particle.INF_MASS,
						410, 200, 0.1
					},
					new List<int> {
						0, 1,
						2, 3,
						4, 5,
						6, 7,
						
						8, 9,
						10, 11,
						12, 13,
						14, 15
					});

			} else if (idx == COLLISIONS2) {
				var p = new List<Particle> {
					new Particle() {Position = new Position(300, 200), Mass = Particle.INF_MASS},
					new Particle() {Position = new Position(110, 200), Mass = 1}
				};
				Enumerable.Range(1, 32).ForEach(i => p.Add(new Particle() {
					Position = new Position(i * (Particle.PARTICLE_RADIUS * 2 + 2), 30),
					Mass = 0.3
				}));
				var c = new List<DistanceConstraint>() {
					new DistanceConstraint(p[0], p[1], Math.Sqrt(p[0].Position.SqDistTo(p[1].Position)))
				};
				return new SpringSystem(p, c);

			} else if (idx == LONG) {
				return generate(
					new List<double> {
						100, 50, 1,
						210, 100, 0.5,
						280, 90, 1,
						350, 100, 0.5,
						450, 150, 1
					},
					new List<int> {
						0, 1,
						1, 2,
						2, 3,
						3, 4
					});

			} else if (idx == BALLS) {
				var p = new List<Particle>();
				Enumerable.Range(1, 35).ForEach(i => p.Add(new Particle() {
					Position = new Position(i * (Particle.PARTICLE_RADIUS * 2 + 1), SpringSystem.GROUND_Y_COORD - i * 10),
					Mass = 1
				}));
				return new SpringSystem(p, new List<DistanceConstraint>());

			} else {
				return new SpringSystem(new List<Particle>(), new List<DistanceConstraint>());
			}
		}





		private static SpringSystem generate(List<double> p, List<int> c) {
			List<Particle> particles = new List<Particle>();
			for (int i = 0; i < p.Count; i += 3) {
				particles.Add(new Particle() {
					Position = new Position(p[i], p[i + 1]),
					Mass = p[i + 2]
				});
			}
			List<DistanceConstraint> constraints = new List<DistanceConstraint>();
			for (int i = 0; i < c.Count; i += 2) {
				constraints.Add(new DistanceConstraint(
					particles[c[i]], particles[c[i + 1]], Math.Sqrt(particles[c[i]].Position.SqDistTo(particles[c[i + 1]].Position))));
			}
			return new SpringSystem(particles, constraints);
		}
	}
}
