﻿using System.Collections.Generic;
using System.Linq;
using ExtendedExtensionMethods;

namespace Animacie_C14 {
	class SpringSystem {

		public const int GROUND_Y_COORD = 400;

		/// <summary>
		/// Scaled for UI units (simulation_distance_unit / simulation_time_unit^2).
		/// </summary>
		public const double GRAVITY_ACCELERATION = 825;


		public List<Particle> Particles { get; private set; }

		public List<DistanceConstraint> Constraints { get; private set; }


		public SpringSystem(List<Particle> particles, List<DistanceConstraint> constraints) {
			particles.ThrowIfNull(); constraints.ThrowIfNull();
			Particles = particles;
			Constraints = constraints;
		}



		/// <summary>
		/// Builds new object with the same values (deep copy).
		/// Necessary to be able to both modify system during simulation and store original system.
		/// </summary>
		public SpringSystem(SpringSystem springSystem) {
			Particles = new List<Particle>(springSystem.Particles.Select(
				p => new Particle() { Position = p.Position, Mass = p.Mass }));
			Constraints = new List<DistanceConstraint>(springSystem.Constraints.Select(c =>
				new DistanceConstraint(Particles[springSystem.Particles.IndexOf(c.p1)], Particles[springSystem.Particles.IndexOf(c.p2)], c.Distance)));
		}



		public Particle GetClosest(Position p) {
			return Particles.IsEmpty() ? null : Particles.MinBy(par => par.Position.SqDistTo(p));
		}

	}
}
