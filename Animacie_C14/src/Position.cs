﻿// ReSharper disable InconsistentNaming

namespace Animacie_C14 {
	class Position {

		public double x;
		public double y;

		public Position(double x, double y) {
			this.x = x;
			this.y = y;
		}

		public static Vector operator -(Position p1, Position p2) {
			return new Vector(p1.x - p2.x, p1.y - p2.y);
		}

		public static Position operator +(Position p, Vector v) {
			return new Position(p.x + v.dx, p.y + v.dy);
		}

		public static Position operator -(Position p, Vector v) {
			return new Position(p.x - v.dx, p.y - v.dy);
		}

		public double SqDistTo(Position p) {
			return (this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y);
		}

		public override string ToString() {
			return "[" + x + ", " + y + "]";
		}


	}
}
