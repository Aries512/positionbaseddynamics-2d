﻿using System;
using ExtendedExtensionMethods;

namespace Animacie_C14 {
	class DistanceConstraint {

		private Particle _p1, _p2;
		public Particle p1 {
			get {
				return _p1;
			}
			set {
				value.ThrowIfNull();
				_p1 = value;
			}
		}
		public Particle p2 {
			get {
				return _p2;
			}
			set {
				value.ThrowIfNull();
				_p2 = value;
			}
		}
		
		private double _distance;
		public double Distance {
			get {
				return _distance;
			}
			set {
				if (value <= 0) {
					throw new ArgumentException("Distance must be greater than zero");
				}
				_distance = value;
			}
		}

		public DistanceConstraint(Particle p1, Particle p2, double sqDistance) {
			p1.ThrowIfNull(); p2.ThrowIfNull();
			if (sqDistance <= 0) {
				throw new ArgumentException("Distance must be greater than zero");
			}
			this._p1 = p1;
			this._p2 = p2;
			this._distance = sqDistance;
		}

		public override string ToString() {
			return "p1: " + p1 + ", p2: " + p2 + ", distance=" + Distance;
		}
	}
}
