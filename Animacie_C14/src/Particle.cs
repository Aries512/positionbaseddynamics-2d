﻿// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Animacie_C14 {
	class Particle {

		public Position Position { get; set; }
		public double Mass { get; set; }
		public const double INF_MASS = double.MaxValue;
		public const double PARTICLE_RADIUS = 7;

		public override string ToString() {
			return "Position=" + Position + ", Mass=" + (Mass == INF_MASS ? "inf" : Mass.ToString("0.###"));
		}

	}
}
