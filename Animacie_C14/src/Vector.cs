﻿using System;
// ReSharper disable InconsistentNaming

namespace Animacie_C14 {
	class Vector {

		public double dx;
		public double dy;

		public Vector(double dx, double dy) {
			this.dx = dx;
			this.dy = dy;
		}

		public static Vector operator *(double val, Vector v) {
			return new Vector(v.dx * val, v.dy * val);
		}
		public static Vector operator *(Vector v, double val) {
			return val * v;
		}

		public static Vector operator /(Vector v, double val) {
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (val == 0) {
				throw new DivideByZeroException();
			}
			return v * (1 / val);
		}

		public static Vector operator +(Vector v1, Vector v2) {
			return new Vector(v1.dx + v2.dx, v1.dy * v2.dy);
		}

		public static Vector operator -(Vector v1, Vector v2) {
			return new Vector(v1.dx - v2.dx, v1.dy - v2.dy);
		}

		public override string ToString() {
			return "(" + dx + ", " + dy + ")";
		}


	}
}
