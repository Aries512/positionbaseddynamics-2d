﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using ExtendedExtensionMethods;
using System.Globalization;
using System.Diagnostics;

namespace Animacie_C14 {

	public partial class MainWindow : Window {



		public MainWindow() {

			InitializeComponent();

			// for . decimal separator and stuff
			// doesn't seem to be working though
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

			sceneDrawingController = new SceneDrawingController(Canvas);
			setShownSystem(getPresetSystem(0));

			simulationSpeedRatio = getValueFromSpeedSlider(sliderSpeed.Value);
			projectionIterationCnt = numIterCnt.Value.Value;
			dampingFactor = numDamping.Value.Value;
		}





		private SpringSystem savedSystem;
		private SpringSystem shownSystem;
		
		private SceneDrawingController sceneDrawingController;



		private void setShownSystem(SpringSystem system) {
			shownSystem = system;
			sceneDrawingController.SetSystem(shownSystem);
			sceneDrawingController.SetStatusText("Stopped");
		}



		private static SpringSystem getPresetSystem(int idx) {
			switch (idx) {
				case 0: return Presets.Get(Presets.ONE_INF);
				case 1: return Presets.Get(Presets.TWO_INF);
				case 2: return Presets.Get(Presets.COLLISIONS1);
				case 3: return Presets.Get(Presets.COLLISIONS2);
				case 4: return Presets.Get(Presets.LONG);
				case 5: return Presets.Get(Presets.BALLS);
				default: return Presets.Get(-1);
			}
		}





		#region DataManipulation

		private void addNewParticle(Particle p) {
			shownSystem.Particles.Add(p);
			sceneDrawingController.AddParticleToUi(p);
		}



		private bool deleteParticle(Particle p) {
			bool deleted = shownSystem.Particles.Remove(p);
			if (deleted) {
				sceneDrawingController.RemoveParticleFromUi(p);
				var toRemoveConstr = shownSystem.Constraints.Where(c => c.p1 == p || c.p2 == p).ToList();
				toRemoveConstr.ForEach(c => sceneDrawingController.RemoveConstraintFromUi(c));
				shownSystem.Constraints.RemoveAll(c => toRemoveConstr.Contains(c));
			}
			return deleted;
		}



		private void moveParticle(Particle particle, Position pos) {
			particle.Position = pos;
			// update constraints for all connected particles
			// ReSharper disable AccessToModifiedClosure
			shownSystem.Constraints.Where(c => c.p1 == particle || c.p2 == particle).ForEach(c =>
				c.Distance = Math.Sqrt(c.p1.Position.SqDistTo(c.p2.Position)));
			sceneDrawingController.UpdateUiAfterParticleChange(particle);
			// ReSharper restore AccessToModifiedClosure
			// don't drag underground
			if (pos.y >= SpringSystem.GROUND_Y_COORD - 1) {
				particle = null;
			}
		}



		private bool tryAddNewConstraint(Particle p1, Particle p2) {
			if (p1 == p2 || shownSystem.Constraints.Where(c => (c.p1 == p1 && c.p2 == p2) || (c.p1 == p2 && c.p2 == p1)).HasContent()) {
				// clicked on self or on other particle with which constraint is already formed; do nothing
				return false;
			} else {
				// add new constraint
				DistanceConstraint c = new DistanceConstraint(p1, p2, Math.Sqrt(p1.Position.SqDistTo(p2.Position)));
				shownSystem.Constraints.Add(c);
				sceneDrawingController.AddConstraintToUi(c);
				return true;
			}
		}

		#endregion




		#region Interaction

		/// <summary>
		/// Returns particle which visually occupies coordinates or null if there is no particle.
		/// </summary>
		private Particle getParticleAt(double x, double y) {
			Position p = new Position(x, y);
			Particle closest = shownSystem.GetClosest(p);
			return closest == null ? null : (closest.Position.SqDistTo(p) < Particle.PARTICLE_RADIUS * Particle.PARTICLE_RADIUS ? closest : null);
		}



		private Particle particleAtCursor = null;

		private bool isCursorOverParticle() {
			return particleAtCursor != null;
		}



		private Particle selectedParticle = null;

		private bool isParticleSelected() {
			return selectedParticle != null;
		}

		/// <summary>
		/// Selects particle, starts constraint adding mode and sets appropriate values in UI controls related to selected particle.
		/// </summary>
		private void selectParticle(Particle p) {

			if (isParticleSelected() && p != selectedParticle) {
				throw new InvalidOperationException("Another particle already selected.");
			}
			
			selectedParticle = p;
			sceneDrawingController.ShowContraintAddingLine();
			
			// we have to disable events raised on changed value to prevent stupid stuff happening
			// like UI controls fighting each other
			unsetParticleUiEvents();
			GroupBoxParticles.IsEnabled = true;
			numParticleX.Value = p.Position.x;
			numParticleY.Value = p.Position.y;
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (p.Mass == Particle.INF_MASS) {
				numParticleMass.IsEnabled = false;
				numParticleMass.Value = null;
				chbxParticleMassInf.IsChecked = true;
			} else {
				numParticleMass.Value = p.Mass;
				numParticleMass.IsEnabled = true;
				chbxParticleMassInf.IsChecked = false;
			}
			setBackParticleUiEvents();
		}



		/// <summary>
		/// Unselects particle, stops constraint line adding and disables UI controls related to particle.
		/// </summary>
		private void unselectParticle() {

			if (!isParticleSelected()) {
				throw new InvalidOperationException("No particle selected.");
			}
			
			sceneDrawingController.StopHighlightParticle(selectedParticle);
			sceneDrawingController.HideContraintAddingLine();
			selectedParticle = null;
			
			unsetParticleUiEvents();
			GroupBoxParticles.IsEnabled = false;
			numParticleX.Value = null;
			numParticleY.Value = null;
			numParticleMass.Value = null;
			chbxParticleMassInf.IsChecked = false;
			setBackParticleUiEvents();
		}



		private void unsetParticleUiEvents() {
			numParticleX.ValueChanged -= numParticleX_ValueChanged;
			numParticleY.ValueChanged -= numParticleY_ValueChanged;
			numParticleMass.ValueChanged -= numParticleMass_ValueChanged;
			chbxParticleMassInf.Checked -= chbxParticleMassInf_Checked;
		}
		private void setBackParticleUiEvents() {
			numParticleX.ValueChanged += numParticleX_ValueChanged;
			numParticleY.ValueChanged += numParticleY_ValueChanged;
			numParticleMass.ValueChanged += numParticleMass_ValueChanged;
			chbxParticleMassInf.Checked += chbxParticleMassInf_Checked;
		}



		private Particle draggedParticle = null;

		private bool isDraggingParticle() {
			return draggedParticle != null;
		}



		// clicked on empty space and nothing is selected: add new particle
		// clicked on existing particle and nothing is selected: select particle and start constraint adding
		// clicked on particle and particle is selected: stop constraint adding if clicked on empty space, add constraint otherwise
		private void Canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {

			if (isPlaybackStarted) {
				return;
			}

			double x = e.GetPosition(Canvas).X, y = e.GetPosition(Canvas).Y;
			Particle p = getParticleAt(x, y);

			if (isParticleSelected()) {
				if (p != null) {
					tryAddNewConstraint(p, selectedParticle);
				}
				unselectParticle();

			} else {
				if (p != null) {
					sceneDrawingController.SetConstraintAddingLineFixedEnd(p.Position.x, p.Position.y);
					sceneDrawingController.SetConstraintAddingLineFreeEnd(x, y);
					selectParticle(p);
					
				} else {
					if (y < SpringSystem.GROUND_Y_COORD - 1) {
						addNewParticle(new Particle() {
							Position = new Position(x, y), Mass = 1
						});
					}
				}
			}
		}



		// allows "click on particle, drag cursor to other, release" to add new constraint
		// if released with cursor over empty space, constraint adding continues and particle is still selected
		private void Canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {

			if (isPlaybackStarted) {
				return;
			}

			double x = e.GetPosition(Canvas).X, y = e.GetPosition(Canvas).Y;
			Particle p = getParticleAt(x, y);

			if (isParticleSelected()) {
				// released over other particle, try add constraint
				if (p != null) {
					tryAddNewConstraint(p, selectedParticle);
					// if we have released over other then selected particle, constraint adding is stopped and selected particle is unselected
					if (p != selectedParticle) {
						unselectParticle();
					}
				}
			}
		}



		// particle selected: unselect selected particle and stop constraint adding
		// cursor over particle and nothing selected: delete particle
		private void Canvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e) {
			if (isPlaybackStarted) {
				return;
			}
			if (!isParticleSelected()) {
				double x = e.GetPosition(Canvas).X, y = e.GetPosition(Canvas).Y;
				Particle p = getParticleAt(x, y);
				if (p != null) {
					deleteParticle(p);
				}
			} else {
				unselectParticle();
			}
		}



		// start dragging on middle click over particle
		private void Canvas_MouseDown(object sender, MouseButtonEventArgs e) {
			if (!isPlaybackStarted && e.ChangedButton == MouseButton.Middle && !isParticleSelected()) {
				double x = e.GetPosition(Canvas).X, y = e.GetPosition(Canvas).Y;
				Particle p = getParticleAt(x, y);
				if (p != null) {
					draggedParticle = p;
				}
			}
		}

		// stop dragging on middle click release
		private void Canvas_MouseUp(object sender, MouseButtonEventArgs e) {
			if (!isPlaybackStarted && e.ChangedButton == MouseButton.Middle && isDraggingParticle()) {
				draggedParticle = null;
			}
		}

		private void Canvas_MouseLeave(object sender, MouseEventArgs e) {
			if (isDraggingParticle()) {
				draggedParticle = null;
			}
		}



		// controls dragging and constraint adding (line drawing)
		private void Canvas_MouseMove(object sender, MouseEventArgs e) {

			if (isPlaybackStarted) {
				return;
			}

			double x = e.GetPosition(Canvas).X, y = e.GetPosition(Canvas).Y;

			if (isDraggingParticle()) {
				moveParticle(draggedParticle, new Position(x, y));

			} else if (isParticleSelected()) {
				sceneDrawingController.SetConstraintAddingLineFreeEnd(x, y);

			} else {
				Particle p = getParticleAt(x, y);

				// no particle on current cursor position
				if (p == null) {
					// stop highlighting old particle, we have moved out of it
					if (isCursorOverParticle()) {
						sceneDrawingController.StopHighlightParticle(particleAtCursor);
					}
					particleAtCursor = null;

				} else { // cursor is over particle
					// moved from empty space to particle, or from one particle to other
					if (particleAtCursor != p) {
						// stop highlighting previous
						if (particleAtCursor != null) {
							sceneDrawingController.StopHighlightParticle(particleAtCursor);
						}
						// handle new particle under cursor
						sceneDrawingController.HighlightParticle(p);
						particleAtCursor = p;
					}
				}
			}
		}



		private void numParticleX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
			if (isParticleSelected() && selectedParticle != null && numParticleX.Value.HasValue) {
				selectedParticle.Position = new Position(numParticleX.Value.Value, selectedParticle.Position.y);
				shownSystem.Constraints.Where(c => c.p1 == selectedParticle || c.p2 == selectedParticle).ForEach(c =>
					c.Distance = Math.Sqrt(c.p1.Position.SqDistTo(c.p2.Position)));
				sceneDrawingController.UpdateUiAfterParticleChange(selectedParticle);
				sceneDrawingController.SetConstraintAddingLineFixedEnd(selectedParticle.Position.x, selectedParticle.Position.y);
			}
		}

		private void numParticleY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
			if (isParticleSelected() && selectedParticle != null && numParticleY.Value.HasValue) {
				selectedParticle.Position = new Position(selectedParticle.Position.x, numParticleY.Value.Value);
				shownSystem.Constraints.Where(c => c.p1 == selectedParticle || c.p2 == selectedParticle).ForEach(c =>
					c.Distance = Math.Sqrt(c.p1.Position.SqDistTo(c.p2.Position)));
				sceneDrawingController.UpdateUiAfterParticleChange(selectedParticle);
				sceneDrawingController.SetConstraintAddingLineFixedEnd(selectedParticle.Position.x, selectedParticle.Position.y);
			}
		}

		private void numParticleMass_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
			if (isParticleSelected() && selectedParticle != null && numParticleMass.Value.HasValue) {
				selectedParticle.Mass = numParticleMass.Value.Value;
				// need to update all to keep correct colors based on particle mass
				sceneDrawingController.UpdateParticleColors();
			}
		}

		private void chbxParticleMassInf_Checked(object sender, RoutedEventArgs e) {
			if (isParticleSelected() && selectedParticle != null && chbxParticleMassInf.IsChecked.HasValue) {
				selectedParticle.Mass = Particle.INF_MASS;
				numParticleMass.Value = null;
				numParticleMass.IsEnabled = false;
				// need to update all to keep correct colors based on particle mass
				sceneDrawingController.UpdateParticleColors();
			}
		}

		private void chbxParticleMassInf_Unchecked(object sender, RoutedEventArgs e) {
			if (isParticleSelected() && selectedParticle != null && chbxParticleMassInf.IsChecked.HasValue) {
				selectedParticle.Mass = 1;
				numParticleMass.IsEnabled = true;
				numParticleMass.Value = 1;
				// need to update all to keep correct colors based on particle mass
				sceneDrawingController.UpdateParticleColors();
			}
		}



		private void btnRemoveAll_Click(object sender, RoutedEventArgs e) {
			if (!isPlaybackStarted) {
				setShownSystem(new SpringSystem(new List<Particle>(), new List<DistanceConstraint>()));
			}
		}

		private void cboxPresets_DropDownClosed(object sender, EventArgs e) {
			if (!isPlaybackStarted) {
				setShownSystem(getPresetSystem(cboxPresets.SelectedIndex));
			}
			
		}

		#endregion





		#region Playback

		/// <summary>
		/// If true, simulation is started (running or paused) and no data can be changed manually.
		/// </summary>
		private volatile bool isPlaybackStarted = false;

		/// <summary>
		/// Whether the simulation is running automatically (without user stepping it manually).
		/// Only used when simulation is already started.
		/// </summary>
		private volatile bool isPlaying = false;
		
		/// <summary>
		/// Stop request for the current playback thread.
		/// Is reset when thread stops.
		/// </summary>
		private volatile bool playbackStopFlag;
		
		/// <summary>
		/// Step request for the current playback thread.
		/// When set to true, causes it to make one step and reset this value back to false.
		/// </summary>
		private volatile bool playbackNextStepManualFlag;

		/// <summary>
		/// Allows playback thread to start only if the previous one have already stopped.
		/// </summary>
		private EventWaitHandle playbackStartEvent = new AutoResetEvent(true);

		/// <summary>
		/// Constraint projection iteration count for PBD.
		/// </summary>
		private int projectionIterationCnt;

		private double dampingFactor;
		
		/// <summary>
		/// Ratio of simulation time to real time.
		/// Simulation time is measured by deltaT passed as an argument when calculating next PBD step.
		/// This means that if ratio is 2, we should during 1 real second calculate 
		/// simulation steps that sum up to 2 deltaT.
		/// </summary>
		private double simulationSpeedRatio;



		private void btnPlayPause_Click(object sender, RoutedEventArgs e) {
			if (!isPlaying) {
				isPlaying = true;
				btnPlayPause.Content = "Pause";
				btnStep.IsEnabled = false;
				sceneDrawingController.SetStatusText("Playing");
				if (!isPlaybackStarted) {
					startPlayback();
				}
			} else {
				isPlaying = false;
				btnPlayPause.Content = "Play";
				btnStep.IsEnabled = true;
				sceneDrawingController.SetStatusText("Paused");
			}
		}

		private void btnStep_Click(object sender, RoutedEventArgs e) {
			if (isPlaybackStarted && !isPlaying) {
				playbackNextStepManualFlag = true;
			}
		}

		private void btnStop_Click(object sender, RoutedEventArgs e) {
			if (isPlaybackStarted) {
				isPlaying = false;
				isPlaybackStarted = false;
				playbackStopFlag = true;
				
				btnStop.IsEnabled = false;
				btnStep.IsEnabled = false;
				btnPlayPause.Content = "Start";
				btnRemoveAll.IsEnabled = true;
				cboxPresets.IsEnabled = true;
				numIterCnt.IsEnabled = true;
				numDamping.IsEnabled = true;
				sceneDrawingController.SetStatusText("Stopped");

				// allow playback thread to finish its stuff
				// never had any problem without waiting, but you never know
				Thread.Sleep(15);
				setShownSystem(savedSystem);
			}
		}

		

		private void startPlayback() {

			setUiAfterPlaybackStart();
			
			Thread thread = new Thread(() => {

				PBDRunner pbd = new PBDRunner(shownSystem, projectionIterationCnt, dampingFactor);
				
				// wait for previous thread to let us start
				playbackStartEvent.WaitOne();

				Stopwatch stopwatch = new Stopwatch();

				// "gameloop"
				// each frame should take 10ms, so the smooth framerate is 100FPS
				// if calculations take too long, simulation is slowed down (it will lag)
				while (true) {
					
					if (playbackStopFlag) {
						playbackStopFlag = false;
						// let other thread (if there is any) start
						playbackStartEvent.Set();
						return;
					}

					stopwatch.Restart();

					if (isPlaying || playbackNextStepManualFlag) {
						Canvas.Dispatcher.Invoke(new Action(() => {
							
							// make 2 steps to make simulation more accurate
							pbd.nextStep(0.005 * simulationSpeedRatio);
							pbd.nextStep(0.005 * simulationSpeedRatio);
							
							if (!playbackStopFlag) { // check if stopped, in case flag is set during simulation computation
								sceneDrawingController.RefreshAll();
							}
						}), null);
						playbackNextStepManualFlag = false;
					}

					stopwatch.Stop();
					// wait for the next frame, but at least 1ms, it results to smoother movement
					int waitNextFrame = Math.Max(1, 10 - (int)stopwatch.ElapsedMilliseconds);
					Thread.Sleep(waitNextFrame);
				}
			});
			
			thread.Start();
		}



		private void setUiAfterPlaybackStart() {
			if (isParticleSelected()) {
				unselectParticle();
			}
			isPlaybackStarted = true;
			savedSystem = new SpringSystem(shownSystem);
			btnStop.IsEnabled = true;
			btnRemoveAll.IsEnabled = false;
			cboxPresets.IsEnabled = false;
			numIterCnt.IsEnabled = false;
			numDamping.IsEnabled = false;
		}



		private void sliderSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
			simulationSpeedRatio = getValueFromSpeedSlider(sliderSpeed.Value);
		}

		private double getValueFromSpeedSlider(double sliderValue) {
			// http://stackoverflow.com/a/17102320/1732455
			// 0: 0 + 0.1
			// 0.5: 1 + 0.1
			// 1: 20 + 0.1
			return 0.1 + 0.055555 * ((Math.Exp(5.888 * sliderValue)) - 1);
		}

		private void numIterCnt_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
			if (numIterCnt.Value.HasValue) {
				projectionIterationCnt = numIterCnt.Value.Value;
			}
		}

		private void numDamping_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
			if (numDamping.Value.HasValue) {
				dampingFactor = numDamping.Value.Value;
			}
		}

		private void Window_Closed(object sender, EventArgs e) {
			// we have to stop playback thread when application is closed
			playbackStopFlag = true;
		}

		#endregion

	}
}
