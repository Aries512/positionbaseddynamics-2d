﻿using System;
using System.Linq;
using ExtendedExtensionMethods;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Animacie_C14 {
	// ReSharper disable once InconsistentNaming
	class PBDRunner {

		private SpringSystem _springSystem;
		public SpringSystem SpringSystem {
			get {
				return _springSystem;
			}
		}



		private int projectionIterations;
		private double dampingFactor;
		private const double PARTICLE_COLLISTION_DIST = Particle.PARTICLE_RADIUS * 2;
		
		// using arrays and indexes to particles to optimize bottleneck in constraint projection
		private Particle[] particles;
		private int[] constrP1Idx;
		private int[] constrP2Idx;
		private double[] constrDist;
		private Vector[] velocities;
		private double[] weights;

		



		public PBDRunner(SpringSystem system, int projectionIterations, double dampingFactor) {
			
			system.ThrowIfNull();
			
			_springSystem = system;
			this.projectionIterations = projectionIterations;
			this.dampingFactor = dampingFactor;
			particles = system.Particles.ToArray();

			DistanceConstraint[] constraints = system.Constraints.Where(
				c => c.p1.Mass != Particle.INF_MASS || c.p2.Mass != Particle.INF_MASS).ToArray();
			constrP1Idx = new int[constraints.Length];
			constrP2Idx = new int[constraints.Length];
			constrDist = new double[constraints.Length];
			constraints.For((c, i) => {
				constrP1Idx[i] = system.Particles.IndexOf(c.p1);
				constrP2Idx[i] = system.Particles.IndexOf(c.p2);
				constrDist[i] = c.Distance;
			});
			velocities = particles.Select(p => new Vector(0, 0)).ToArray();
			weights = particles.Select(p => p.Mass == Particle.INF_MASS ? 0 : 1 / p.Mass).ToArray();
		}





		public void nextStep(double deltaT) {

			// apply gravity and damping
			particles.For((p, i) => {
				if (p.Mass != Particle.INF_MASS) {
					velocities[i].dy += deltaT * SpringSystem.GRAVITY_ACCELERATION;
					velocities[i] *= Math.Pow(dampingFactor, deltaT); // scale damping accordingly to step size
					// TODO: better damping?
				}
			});

			
			// calculated positions in this round
			Position[] pos = particles.Select((p, i) => p.Position + (deltaT * velocities[i])).ToArray();
			

			for (int iter = 0; iter < projectionIterations; ++iter) {
				
				// distance constraints
				constrP1Idx.For((_, i) => projectConstraint(constrP1Idx[i], constrP2Idx[i], constrDist[i], pos));
				
				// collisions, implemented as distace constraints when particles overlap
				particles.For((p1, i1) => {
					particles.For((p2, i2) => {
						if (p1 != p2 && pos[i1].SqDistTo(pos[i2]) < PARTICLE_COLLISTION_DIST * PARTICLE_COLLISTION_DIST) {
							projectConstraint(i1, i2, PARTICLE_COLLISTION_DIST, pos);
						}
					});
				});
			}


			particles.For((p, i) => {
				
				velocities[i] = (pos[i] - p.Position) / deltaT;
				
				// bouncing off the ground
				if (pos[i].y > SpringSystem.GROUND_Y_COORD) {
					pos[i].y = SpringSystem.GROUND_Y_COORD;
					velocities[i].dy *= -0.7;
				}
				
				if (p.Mass != Particle.INF_MASS) {
					p.Position = pos[i];
				}
			});

		}





		private void projectConstraint(int p1Idx, int p2Idx, double dist, Position[] pos) {
			double pDist = Math.Sqrt(pos[p1Idx].SqDistTo(pos[p2Idx]));
			Vector deltaUnweighted = (pDist - dist) * (pos[p1Idx] - pos[p2Idx]) / pDist;
			Vector delta1 = (-weights[p1Idx] / (weights[p1Idx] + weights[p2Idx])) * deltaUnweighted;
			pos[p1Idx] += delta1;
			Vector delta2 = (weights[p2Idx] / (weights[p1Idx] + weights[p2Idx])) * deltaUnweighted;
			pos[p2Idx] += delta2;
		}


	}
}
