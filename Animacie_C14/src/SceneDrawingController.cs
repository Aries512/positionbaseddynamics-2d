﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ExtendedExtensionMethods;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Animacie_C14 {
	class SceneDrawingController {



		private Canvas canvas;
		
		private List<Particle> particles;
		private List<DistanceConstraint> constraints;
		
		private Dictionary<Particle, Ellipse> particleUiElements;
		private Dictionary<DistanceConstraint, Line> constraintUiElements;

		private TextBlock statusText;
		private Line constraintAddingLine = new Line() {
			Stroke = defaultParticleBrush,
			Visibility = Visibility.Collapsed
		};





		public SceneDrawingController(Canvas canvas) {
			canvas.ThrowIfNull();
			this.canvas = canvas;
		}



		public void SetSystem(SpringSystem system) {
			reset();
			particles = system.Particles;
			particles.ForEach(AddParticleToUi);
			constraints = system.Constraints;
			constraints.ForEach(AddConstraintToUi);
		}

		private void reset() {
			clearUi();
			particles = new List<Particle>();
			constraints = new List<DistanceConstraint>();
			particleUiElements = new Dictionary<Particle, Ellipse>();
			constraintUiElements = new Dictionary<DistanceConstraint, Line>();
		}

		private void clearUi() {
			canvas.Children.Clear();
			canvas.Children.Add(constraintAddingLine);
			drawGround();
			addStatusText();
		}





		private void drawGround() {
			var rect = new Rectangle() {
				Width = 10000,
				Height = 10000,
				Margin = new Thickness(0, SpringSystem.GROUND_Y_COORD + Particle.PARTICLE_RADIUS / 2 + 1, 0, 0),
				Fill = new SolidColorBrush(Colors.LightGray)
			};
			canvas.Children.Add(rect);
			Line line = new Line() {
				X1 = 0,
				Y1 = SpringSystem.GROUND_Y_COORD + Particle.PARTICLE_RADIUS / 2 + 1,
				X2 = 10000,
				Y2 = SpringSystem.GROUND_Y_COORD,
				StrokeThickness = 2,
				Stroke = new SolidColorBrush(Colors.Gray)
			};
			canvas.Children.Add(line);
			Canvas.SetZIndex(rect, -1);
			Canvas.SetZIndex(line, -1);
		}



		private void addStatusText() {
			canvas.Children.Add(statusText = new TextBlock() {
				Text = "",
				FontSize = 18,
				FontWeight = FontWeights.Bold,
				Foreground = new SolidColorBrush(Color.FromRgb(35, 35, 35)),
				Margin = new Thickness(5, SpringSystem.GROUND_Y_COORD + 10, 0, 5)
			});
		}

		public void SetStatusText(string text) {
			statusText.Text = text;
		}



		public void AddParticleToUi(Particle p) {
			Ellipse e = new Ellipse() {
				Margin = getEllipseMargin(p),
				Width = Particle.PARTICLE_RADIUS * 2, Height = Particle.PARTICLE_RADIUS * 2,
				Fill = getParticleEllipseBrush(p)
			};
			particleUiElements.Add(p, e);
			canvas.Children.Add(e);
			UpdateParticleColors();
		}

		private Thickness getEllipseMargin(Particle p) {
			return new Thickness(p.Position.x - Particle.PARTICLE_RADIUS, p.Position.y - Particle.PARTICLE_RADIUS, 0, 0);
		}



		public void RemoveParticleFromUi(Particle p) {
			Ellipse e;
			if (particleUiElements.TryGetValue(p, out e)) {
				particleUiElements.Remove(p);
				canvas.Children.Remove(e);
			}
			UpdateParticleColors();
		}



		public void HighlightParticle(Particle p) {
			Ellipse e;
			if (particleUiElements.TryGetValue(p, out e)) {
				e.Stroke = new SolidColorBrush(Colors.Red);
			}
		}

		public void StopHighlightParticle(Particle p) {
			Ellipse e;
			if (particleUiElements.TryGetValue(p, out e)) {
				e.Stroke = null;
			}
		}



		public void AddConstraintToUi(DistanceConstraint c) {
			Line line = new Line() {
				X1 = c.p1.Position.x, Y1 = c.p1.Position.y,
				X2 = c.p2.Position.x, Y2 = c.p2.Position.y,
				Stroke = defaultParticleBrush
			};
			constraintUiElements.Add(c, line);
			canvas.Children.Add(line);
		}

		public void RemoveConstraintFromUi(DistanceConstraint c) {
			Line line;
			if (constraintUiElements.TryGetValue(c, out line)) {
				constraintUiElements.Remove(c);
				canvas.Children.Remove(line);
			}
		}



		public void UpdateParticleColors() {
			particles.ForEach(p => {
				Ellipse e;
				if (particleUiElements.TryGetValue(p, out e)) {
					e.Fill = getParticleEllipseBrush(p);
				}
			});
		}



		public void UpdateUiAfterParticleChange(Particle p) {
			Ellipse e;
			if (particleUiElements.TryGetValue(p, out e)) {
				e.Margin = getEllipseMargin(p);
				e.Fill = getParticleEllipseBrush(p);
			}
			constraints.Where(c => c.p1 == p || c.p2 == p).ForEach(c => {
				Line line;
				if (constraintUiElements.TryGetValue(c, out line)) {
					line.X1 = c.p1.Position.x; line.Y1 = c.p1.Position.y;
					line.X2 = c.p2.Position.x; line.Y2 = c.p2.Position.y;
				}
			});
		}



		public void RefreshAll() {
			particles.ForEach(UpdateUiAfterParticleChange);
		}



		private Brush getParticleEllipseBrush(Particle p) {
			if (p.Mass == Particle.INF_MASS) {
				return infiniteMassParticleBrush;
			}
			var masses = particles.Where(q => q.Mass != Particle.INF_MASS).Select(q => q.Mass).ToList();
			// all masses same
			if (!masses.Distinct().Skip(1).Any()) {
				return defaultParticleBrush;
			}
			// scale grey from 50 (heaviest) to 125 (lightest)
			byte shade = (byte)((1 - p.Mass / masses.Max()) * 135 + 50);
			return new SolidColorBrush(Color.FromArgb(255, shade, shade, shade));
		}

		private static readonly Brush defaultParticleBrush = new SolidColorBrush(Color.FromArgb(255, 50, 50, 50));
		private static readonly Brush infiniteMassParticleBrush = makeInfiniteMassParticleBrush();

		// x in circle
		private static Brush makeInfiniteMassParticleBrush() {
			const double size = Particle.PARTICLE_RADIUS * 2;
			GeometryGroup gg = new GeometryGroup();
			gg.Children.Add(new LineGeometry(new Point(0, 0), new Point(size, size)));
			gg.Children.Add(new LineGeometry(new Point(0, size), new Point(size, 0)));
			gg.Children.Add(new EllipseGeometry(new Rect(new Size(size, size))));
			Drawing drawing = new GeometryDrawing(
				Brushes.Transparent,
				new Pen(defaultParticleBrush, 2),
				gg);
			return new DrawingBrush(drawing);
		}





		public void ShowContraintAddingLine() {
			constraintAddingLine.Visibility = Visibility.Visible;
		}

		public void HideContraintAddingLine() {
			constraintAddingLine.Visibility = Visibility.Collapsed;
		}

		public void SetConstraintAddingLineFixedEnd(double x, double y) {
			constraintAddingLine.X1 = x;
			constraintAddingLine.Y1 = y;
		}
		public void SetConstraintAddingLineFreeEnd(double x, double y) {
			constraintAddingLine.X2 = x;
			constraintAddingLine.Y2 = y;
		}



	}
}
