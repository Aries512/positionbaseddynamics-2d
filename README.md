# Position based dynamics 2D #
Simple 2D physical simulation demo created as school project.
Based on [this paper](matthias-mueller-fischer.ch/publications/posBasedDyn.pdf). Uses only simple infinitely stiff distance constraints between particles with no friction or restitution.